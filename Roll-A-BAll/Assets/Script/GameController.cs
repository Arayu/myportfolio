﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public UnityEngine.UI.Text scoreLabel;
    public GameObject winnerLabelObject;

    // Update is called once per frame
    public void Update()
    {
        int count = GameObject.FindGameObjectsWithTag("Item").Length;
        scoreLabel.text = count.ToString();

        Scene scene = SceneManager.GetActiveScene();
        int sceneIndex = scene.buildIndex;
        Debug.Log(sceneIndex);
        //int countScene = SceneManager.sceneCount;
        //Debug.Log(countScene);
        //SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        //Debug.Log(countScene);

        if (count == 0 && sceneIndex == 0)

        {
            // オブジェクトをアクティブにする
            winnerLabelObject.SetActive(true);

        }
        else if(count == 0 && sceneIndex == 1)
        {
            sceneIndex--;
            winnerLabelObject.SetActive(true);
            // SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);
        }

    }
}
