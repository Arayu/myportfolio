﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piston : MonoBehaviour
{
    bool m_zPlus = true; // z軸プラス方向に移動中
    Vector3 vecBasePos;

    private void Start()
    {
        // use this for initialization
        vecBasePos = transform.position;
    }

    private void Update()
    {
        if (m_zPlus)
        {
            transform.position += new Vector3(0f, 0f, 2f * Time.deltaTime);
            if (transform.position.z - vecBasePos.z >= 3)
                m_zPlus = false;
        }
        else
        {
            transform.position -= new Vector3(0f, 0f, 2f * Time.deltaTime);
            if (transform.position.z - vecBasePos.z <= -3)
                m_zPlus = true;
        }
    }
}