﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    // speedを制御する
    public float speed = 10;
    public float flap = 1000f;
    bool jump = false;

    void FixedUpdate()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Rigidbody rigidbody = GetComponent<Rigidbody>();

        // xとzにspeedを掛ける
        rigidbody.AddForce(x * speed, 0, z * speed);
        if (Input.GetKeyDown("space") && !jump)
        {
            rigidbody.AddForce(Vector3.up * flap);
            jump = true;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            jump = false;
        }
    }
}