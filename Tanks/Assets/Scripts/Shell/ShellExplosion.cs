﻿using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    public LayerMask m_TankMask;
    public ParticleSystem m_ExplosionParticles;
    public AudioSource m_ExplosionAudio;
    public float m_MaxDamage = 50f;
    public float m_ExplosionForce = 1000f;
    public float m_MaxLifeTime = 2f;
    public float m_ExplosionRadius = 5f;


    private void Start()
    {
        Destroy(gameObject, m_MaxLifeTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        // Find all the tanks in an area around the shell and damage them in the defined sphere.
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();


            // 処理1行の時のif文で{}省略、球判定で見つかったcollidersのRigidbody型データtargetRigidbodyを
            // 取得手出来ていなかった場合には以降の処理を行わずに次のループに移る
            if (!targetRigidbody)
                continue;

            // Tankに力を加えて動かす
            targetRigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_ExplosionRadius);

            // TankHealth型（TankHealthのインスタンス生成と同義）変数targetHealthを定義して、HPを取得
            TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();
            if (!targetHealth) continue;
            float damage = CalculateDamage(targetRigidbody.position);
            targetHealth.TakeDamage(damage);

        }
        /* Delayを入れてない場合は下記の処理は同時に実行される。
           Shellを消す処理の際に子のparticleも消されてしまう。
           よって、先にPariticlesをShellの子から外す。
           Pariticlesの動作完了まではParticlesを消したくないのでDelayを入れる
           */
        m_ExplosionParticles.transform.parent = null;
        m_ExplosionParticles.Play();
        m_ExplosionAudio.Play();
        Destroy(m_ExplosionParticles.gameObject, m_ExplosionParticles.duration);
        Destroy(gameObject);
    }



    private float CalculateDamage(Vector3 targetPosition)
    {
        // Calculate the amount of damage a target should take based on it's position.
        Vector3 explosionToTarget = targetPosition - transform.position;
        float explosionDistance = explosionToTarget.magnitude;
        float relativeDistance = (m_ExplosionRadius - explosionDistance) / m_ExplosionRadius;
        float damage = relativeDistance * m_MaxDamage;
        damage = Mathf.Max(0f, damage);
        return damage;

    }
}