<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RankingtestTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RankingtestTable Test Case
 */
class RankingtestTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RankingtestTable
     */
    public $Rankingtest;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Rankingtest'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Rankingtest') ? [] : ['className' => RankingtestTable::class];
        $this->Rankingtest = TableRegistry::getTableLocator()->get('Rankingtest', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Rankingtest);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
