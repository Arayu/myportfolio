<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ranking Controller
 *
 * @property \App\Model\Table\RankingTable $Ranking
 *
 * @method \App\Model\Entity\Ranking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RankingController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $ranking = $this->paginate($this->Ranking);

        $this->set(compact('ranking'));
    }

    /**
     * View method
     *
     * @param string|null $id Ranking id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ranking = $this->Ranking->get($id, [
            'contain' => []
        ]);

        $this->set('ranking', $ranking);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ranking = $this->Ranking->newEntity();
        if ($this->request->is('post')) {
            $ranking = $this->Ranking->patchEntity($ranking, $this->request->getData());
            if ($this->Ranking->save($ranking)) {
                $this->Flash->success(__('The ranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ranking could not be saved. Please, try again.'));
        }
        $this->set(compact('ranking'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Ranking id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ranking = $this->Ranking->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ranking = $this->Ranking->patchEntity($ranking, $this->request->getData());
            if ($this->Ranking->save($ranking)) {
                $this->Flash->success(__('The ranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ranking could not be saved. Please, try again.'));
        }
        $this->set(compact('ranking'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Ranking id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ranking = $this->Ranking->get($id);
        if ($this->Ranking->delete($ranking)) {
            $this->Flash->success(__('The ranking has been deleted.'));
        } else {
            $this->Flash->error(__('The ranking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getRankings()
	{
		$this->autoRender	= false;
		
		//テーブルからランキングリストをとってくる
        $query	= $this->Ranking->find("all");

        //クエリー処理を行う。
        $query->where(['Stage'=>1])->order(['Score'=>'DESC']);   //降順
        $query->limit(15);                  //取得件数を絞る
		
		//jsonにシリアライズする。
		$json	= json_encode($query);

		//jsonデータを返す。（レスポンスとして表示する。）
		echo "{\"Lines\":" . $json ."}";
    }

    public function setRanking()
    {
		$this->autoRender	= false;

        //POST パラメータを取得
        $postName   = $this->request->data("Name");
        $postScore  = $this->request->data("Score");
        $postStage  = $this->request->data("Stage");

        $record = array(
            "Name"=>$postName,
            "Score"=>$postScore,
            "Created"=>date("Y/m/d H:i:s"),
            "Stage"=>$postStage
        );

        //テーブルにレコードを追加
        $prm1    = $this->Ranking->newEntity();
        $prm2    = $this->Ranking->patchEntity($prm1,$record);
        
        if( $this->Ranking->save($prm2) ){
            echo "1";   //成功
        }else{
            echo "0";   //失敗
        }
    }
}
