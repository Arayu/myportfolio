<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rankingtest Model
 *
 * @method \App\Model\Entity\Rankingtest get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rankingtest newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rankingtest[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rankingtest|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rankingtest saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rankingtest patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rankingtest[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rankingtest findOrCreate($search, callable $callback = null, $options = [])
 */
class RankingtestTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rankingtest');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Id')
            ->allowEmptyString('Id', null, 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 32)
            ->requirePresence('Name', 'create')
            ->notEmptyString('Name');

        $validator
            ->integer('Level')
            ->requirePresence('Level', 'create')
            ->notEmptyString('Level');

        $validator
            ->integer('EquipId')
            ->requirePresence('EquipId', 'create')
            ->notEmptyString('EquipId');

        return $validator;
    }
}
