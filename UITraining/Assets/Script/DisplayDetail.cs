﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DisplayDetail : MonoBehaviour,  IPointerDownHandler
{
    public GameObject SubMenuDetail;


    ///  {
    ///     SubMenuDetail.SetActive(false);
    ///  }


    // Update is called once per frame
    public void Display()
    {
        SubMenuDetail.SetActive(true);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        SubMenuDetail.SetActive(false);
    }
}
