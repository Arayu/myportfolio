﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DisplaySlider : MonoBehaviour, IPointerDownHandler
{
    public GameObject controllSlider;


    ///  {
    ///     SubMenuDetail.SetActive(false);
    ///  }


    // Update is called once per frame
    public void Display()
    {
        controllSlider.SetActive(true);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        controllSlider.SetActive(false);
    }
}
