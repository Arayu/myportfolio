// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BlockbreakerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BLOCKBREAKER_API ABlockbreakerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
