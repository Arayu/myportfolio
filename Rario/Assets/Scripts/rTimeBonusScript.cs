﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class rTimeBonusScript : MonoBehaviour
{

    public Text rBonusScore;
    //private ScoreScript _score;

    // Start is called before the first frame update
    void Start()
    {
        // _score = GameObject.Find("Score").GetComponent<ScoreScript>();
        //Debug.Log(ScoreScript.BonusScore);
    }

    // Update is called once per frame
    void Update()
    {
        rBonusScore.text = string.Format("{0:00000}", ScoreScript.BonusScore);
    }
}
