﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class rScoreScript : MonoBehaviour
{

    public Text rScoreText;
    // private ScoreScript _score;

    // Start is called before the first frame update
    void Start()
    {
        //   _score = GameObject.Find("Score").GetComponent<ScoreScript>();
    }

    // Update is called once per frame
    void Update()
    {
        rScoreText.text = string.Format("{0:00000}", ScoreScript.score);
    }
}
