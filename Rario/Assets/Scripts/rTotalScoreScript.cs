﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class rTotalScoreScript : MonoBehaviour
{
    public Text rTotalScore;
    //private ScoreScript _score;
    public static int totalScore;

    // Start is called before the first frame update
    void Start()
    {
        //_score = GameObject.Find("Score").GetComponent<ScoreScript>();
    }

    // Update is called once per frame
    void Update()
    {
        totalScore = ScoreScript.score + ScoreScript.BonusScore;
        rTotalScore.text = string.Format("{0:00000}", totalScore);

    }
}
