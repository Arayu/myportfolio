﻿using UnityEngine;
using System.Collections;

public class Enemy1Script : MonoBehaviour
{

    Rigidbody2D rigidbody2D;
    public int speed = -3;
    public GameObject explosion;
    public GameObject item;
    public int attackPoint = 10;
    private LifeScript lifeScript;
    //メインカメラのタグ名　constは定数(絶対に変わらない値)
    private const string MAIN_CAMERA_TAG_NAME = "MainCamera";
    //カメラに映っているかの判定
    private bool _isRendered = false;
    private ScoreScript _scoreScript;

    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        lifeScript = GameObject.FindGameObjectWithTag("HP").GetComponent<LifeScript>();
        _scoreScript = GameObject.Find("Score").GetComponent<ScoreScript>();
    }

    void Update()
    {
        if (_isRendered)
        {
            rigidbody2D.velocity = new Vector2(speed, rigidbody2D.velocity.y);
        }

        if (gameObject.transform.position.y < Camera.main.transform.position.y - 8 ||
                gameObject.transform.position.x < Camera.main.transform.position.x - 10)
        {
            Destroy(gameObject);
        }

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (_isRendered)
        {
            if (col.tag == "Bullet")
            {
                Destroy(gameObject);
                Instantiate(explosion, transform.position, transform.rotation);
                //5分の1の確率で回復アイテムを落とす
                if (Random.Range(0, 8) == 0)
                {
                    Instantiate(item, transform.position, transform.rotation);
                }
                _scoreScript.scoreUp = 100;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "UnityChan")
        {
            lifeScript.LifeDown(attackPoint);
        }
    }

    //Rendererがカメラに映ってる間に呼ばれ続ける
    void OnWillRenderObject()
    {
        //メインカメラに映った時だけ_isRenderedをtrue
        if (Camera.current.tag == MAIN_CAMERA_TAG_NAME)
        {
            _isRendered = true;
        }
    }
}