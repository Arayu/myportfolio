﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{


    public Text scoreText; // Text用変数
    public static int score = 0; // スコア計算用変数
    public int scoreUp = 0; //敵を倒したり、アイテム取得時のスコア上昇値
    public static int BonusScore;
    public GameObject Timer;
    //private Player _player;
    public GameObject UnityChan;


    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "Score : 0"; //初期スコアを代入して画面表示
        //_timer = Timer.GetComponent<TimerScript>();
        //_player = GameObject.Find("UnityChan").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {

        score += scoreUp;
        scoreText.text = string.Format("Score : {0:00000}", score);
        scoreUp = 0;

        if (UnityChan.GetComponent<Player>().getGameClear() == true)
        {
            BonusScore = (100 - Timer.GetComponent<TimerScript>().seconds) * 20;
            // totalScore = score + BonusScore;
            // BonusScore = 0;


        }

    }
    /*
    public static int getScore()
    {
        return score;
    }

    public static int getBonusScore()
    {
        return BonusScore;
    }
    */
}

