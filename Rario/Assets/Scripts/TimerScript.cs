﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    public Text timerText;
    public float totalTime = 0;
    public int seconds;
    //private Player _player;
    public GameObject UnityChan;

    // Start is called before the first frame update
    void Start()
    {
        // _player = GameObject.Find("UnityChan").GetComponent<Player>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (UnityChan.GetComponent<Player>().getGameClear() != true)
        {
            totalTime += Time.deltaTime;
            seconds = (int)totalTime;
            timerText.text = string.Format("Time  : {0:000} s", seconds);
        }

    }
}
