﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;	//Text
using MiniJSON;		// Json
using System;		// File
using System.IO;	// File
using System.Text;	// File
using UnityEngine.Networking;
public class GetRankingScript : MonoBehaviour
{

    [SerializeField] private GameObject RankingNode;
    [SerializeField] private GameObject _content;
    List<RankingDataScript> rankingList = null;

    void Start()
    {
        GetJsonFromWebRequest();
    }


    private void GetJsonFromWebRequest()
    {
        const string url = "http://localhost/RarioAPI/Ranking/getRankings";

        StartCoroutine(GetRanking(url, CallBackSuccess, CallBackFailed));
    }

    private void CallBackSuccess(string text)
    {
        Debug.Log(text);
        rankingList = RankingDataModelScript.DesirializeFromJson(text);
        ShowRanking();
    }

    private void CallBackFailed()
    {
        Debug.Log("Failed");
    }

    private IEnumerator GetRanking(string url, Action<string> callBackSuccess, Action callBackFailed = null)
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(url);

        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            if (callBackFailed != null)
            {
                callBackFailed();
            }
        }
        else if (webRequest.isDone)
        {
            if (callBackSuccess != null)
            {
                callBackSuccess(webRequest.downloadHandler.text);
            }
        }
    }

    public void ShowRanking()
    {
        string sStrOutput = "";

        if (null == rankingList)
        {
            sStrOutput = "no list !";
        }
        else
        {
            int index = 0;
            foreach (var rankingData in rankingList)
            {
                ++index;
                GameObject rankingNode = Instantiate(RankingNode);

                RankingNodeScript tmp = rankingNode.GetComponent<RankingNodeScript>();
                tmp.SetNodeText(index, rankingData.Name, rankingData.Score);


                //       += rankingData.Name;
                //rankingScoreComp.text += rankingData.Score;

                //表示したい内容
                //index;
                //rankingData.Score;
                //rankingData.Name;

                //rankingNodeをContentの下につけたい
                rankingNode.transform.SetParent(_content.transform);
            }
            //rankingScoreComp.text = sStrOutput;
        }

    }

}