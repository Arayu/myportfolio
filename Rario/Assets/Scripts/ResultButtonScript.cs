﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ResultButtonScript : MonoBehaviour
{
    [SerializeField]
    private Text playerNameComp;

    public void OnClickGetButton()
    {
        GetJsonFromWebRequest();
    }

    public void OnClickSetButton()
    {
        SetJsonFromWebRequest();
    }

    private void GetJsonFromWebRequest()
    {
        const string url = "http://localhost/RarioAPI/Ranking/getRankings";


        StartCoroutine(GetRanking(url, CallBackSuccess, CallBackFailed));
    }

    private void SetJsonFromWebRequest()
    {
        const string url = "http://localhost/RarioAPI/Ranking/setRanking";

        StartCoroutine(SetRanking(url, CallBackSuccess, CallBackFailed));
    }

    private void CallBackSuccess(string text)
    {
        Debug.Log(text);
    }

    private void CallBackFailed()
    {
        Debug.Log("Failed");
    }

    private IEnumerator GetRanking(string url, Action<string> callBackSuccess, Action callBackFailed = null)
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(url);

        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            if (callBackFailed != null)
            {
                callBackFailed();
            }
        }
        else if (webRequest.isDone)
        {
            if (callBackSuccess != null)
            {
                callBackSuccess(webRequest.downloadHandler.text);
            }
        }
    }

    private IEnumerator SetRanking(string url, Action<string> callBackSuccess, Action callBackFailed = null)
    {
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("Name", playerNameComp.text);
        wwwForm.AddField("Score", rTotalScoreScript.totalScore);
        wwwForm.AddField("Stage", "1");


        UnityWebRequest webRequest = UnityWebRequest.Post(url, wwwForm);

        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            if (callBackFailed != null)
            {
                callBackFailed();
            }
        }
        else if (webRequest.isDone)
        {
            if (callBackSuccess != null)
            {
                callBackSuccess(webRequest.downloadHandler.text);

                Application.LoadLevel("Ranking");

            }
        }
    }
}