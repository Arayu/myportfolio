﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerScript : MonoBehaviour
{
    [SerializeField]private GameObject audioManager;
    [SerializeField]private AudioClip audioClip1;
    [SerializeField]private AudioClip audioClip2;
    [SerializeField]private GameObject Hp;
    [SerializeField]private GameObject UnityChan;
    bool isCalledOnce = false;
    private AudioSource audioSource;


    //Gameover時のAudio切替え
    public void ChangeEndAudio()
    {
        audioSource = audioManager.GetComponent<AudioSource>();
        audioSource.clip = audioClip1;
        audioSource.loop = false;
        audioSource.Play();
    }

    //Gameclear時のAudio切替え
    public void ChangeClearAudio()
    {
        audioSource = audioManager.GetComponent<AudioSource>();
        audioSource.clip = audioClip2;
        audioSource.loop = false;
        audioSource.Play();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isCalledOnce == false)
        {
            if (Hp.GetComponent<LifeScript>().getGameOver() == true)
            {
                ChangeEndAudio();
                isCalledOnce = true;
            }

            else if (UnityChan.GetComponent<Player>().getGameClear() == true)
            {
                ChangeClearAudio();
                isCalledOnce = true;
            }
        }
    }
}
