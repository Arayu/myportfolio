﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RankingDataScript
{
    public string Name;
    public int Score;
}

[Serializable]
public class RankingDataScriptList
{
    public List<RankingDataScript> Lines;
}
