﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingNodeScript : MonoBehaviour
{
    [SerializeField] private Text _rankingIndexText;
    [SerializeField] private Text _rankingNameText;
    [SerializeField] private Text _rankingScoreText;

    public void SetNodeText(int No, string Name, int Score)
    {
        _rankingIndexText.text = No.ToString();
        _rankingNameText.text = Name;
        _rankingScoreText.text = Score.ToString();

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {



    }
}
